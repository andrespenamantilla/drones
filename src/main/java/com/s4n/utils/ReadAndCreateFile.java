package com.s4n.utils;

import com.s4n.constants.Constants;
import com.s4n.pojo.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

/**
 * CLase que lee y escribe archivos
 *
 * @author Andrés Peña Mantilla
 */

public class ReadAndCreateFile {

    //Método que por medio de un indice lee el archivo de entrada correspondiente y devuelve una linea de comandos a ejecutar
    public List<String> readTxt(Integer index, String inputfolder) {

        String nameIndex = nameIndex(index);
        String path = Constants.FILES_FOLDER + File.separator + inputfolder + File.separator + Constants.IN_SUFFIX + nameIndex + Constants.EXTENTION;
        try {
            List<String> commandRoute = new ArrayList<>();
            File file = new File(path);
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine())
                commandRoute.add(sc.nextLine());

            return commandRoute;
        } catch (Exception e) {

        }

        return new ArrayList<>();
    }

    //Método que por medio de un indice y una lista de posiciones ( coordenadas ), crea el archivo outXX en el directorio de salidas
    public void createTxt(Integer index, List<Position> positions, String outputfolder) throws FileNotFoundException, UnsupportedEncodingException {
        String nameIndex = nameIndex(index);
        String path = Constants.FILES_FOLDER + File.separator + outputfolder + File.separator + Constants.OUT_SUFFIX + nameIndex + Constants.EXTENTION;

        File file = new File(path);

        if (file.exists()) {
            file.delete();
        }

        PrintWriter writer = new PrintWriter(path, "UTF-8");
        writer.println(Constants.HEAD_REPORT);
        AtomicReference<String> txtLine= new AtomicReference<>("");
        positions.forEach(line->{
            txtLine.set("(" + line.getX_value() + "," + line.getY_value() + ") " + line.getDirection());
            writer.println("("+line.getX_value()+","+line.getY_value()+") " +line.getDirection());
        });
        writer.close();

    }

    //Metodo que obtiene el indice correcto para crear o leer el documento
    public String nameIndex(Integer index) {
        String nameIndex = "";

        if (index > 0 && index <= 9) {
            nameIndex = "0" + index.toString();
        }
        if (index >= 10) {
            nameIndex = index.toString();
        }

        return nameIndex;
    }
}
