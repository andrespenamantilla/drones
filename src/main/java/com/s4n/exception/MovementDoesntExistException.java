package com.s4n.exception;


/**
 *Excepción para cuando el movimiento no existe en el enum de los comandos de movimiento
 *
 * @author Andrés Peña Mantilla
 */
public class MovementDoesntExistException extends Exception{

    public static final String MESSAGE = "THE MOVEMENT THAT THE FILE HAS, ARE NOT REGISTERED";

    public MovementDoesntExistException() {
        super(MESSAGE);
    }
}
