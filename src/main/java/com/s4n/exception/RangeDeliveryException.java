package com.s4n.exception;


/**
 * Excepción para cuando el dron excede el rango de alcance para los delivery
 * @author Andrés Peña Mantilla
 */
public class RangeDeliveryException extends Exception {
    public static final String MESSAGE = "THE DRONE CANT DELIVER , THE RANGE IS TOO MANY LARGE";
    
    public RangeDeliveryException() {
        super(MESSAGE);
    }

}
