package com.s4n.exception;


/**
 * Excepción para cuando el dron lleva mas de tres almuerzos para delivery
 *
 * @author Andrés Peña Mantilla
 */
public class NumberOfDinnerException extends Exception {

    public static final String MESSAGE = "TO MANY DINNERS FOR DELIVERY";

    public NumberOfDinnerException() {
        super(MESSAGE);
    }
}
