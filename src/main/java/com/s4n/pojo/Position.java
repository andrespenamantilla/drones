package com.s4n.pojo;

/**
 * Clase  pojo que se encarga de almacenar las variables de las coordenadas X y Y y la dirección del dron.
 *
 * @author Andrés Peña Mantilla
 */
public class Position {

    public Integer x_value;
    public Integer y_value;
    public String direction;

    public Position(Integer x_value, Integer y_value, String direction) {
        this.x_value = x_value;
        this.y_value = y_value;
        this.direction = direction;
    }

    public Position() {
    }

    public Integer getX_value() {
        return x_value;
    }

    public void setX_value(Integer x_value) {
        this.x_value = x_value;
    }

    public Integer getY_value() {
        return y_value;
    }

    public void setY_value(Integer y_value) {
        this.y_value = y_value;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
