package com.s4n.constants;


/**
 * Enum que contiene la lista de movimientos que existen en el enunciado del problema
 *
 * @author Andrés Peña Mantilla
 */
public enum MovementCommand {
    D,I,A;

    public static boolean contains(String value) {

        for (MovementCommand m : MovementCommand.values()) {
            if (m.name().equals(value)) {
                return true;
            }
        }

        return false;
    }

}
