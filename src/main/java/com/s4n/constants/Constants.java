package com.s4n.constants;

/**
 * Clase que se usa para referenciación de las constantes utilizadas en la app
 *
 * @author Andrés Peña Mantilla
 */
public class Constants {

    public final static String UP = "dirección Norte";
    public final static String DOWN = "dirección Sur";
    public final static String RIGHT = "dirección Oriente" ;
    public final static String LEFT = "dirección Occidente";
    public final static Integer RANGE_DELIVERY = 10;
    public final static Integer NUMBER_OF_DRONES = 20;
    public final static String FILES_FOLDER = "files";
    public final static String INPUT_FOLDER = "input";
    public final static String OUTPUT_FOLDER = "output";
    public final static String EXTENTION = ".txt";
    public static final String HEAD_REPORT = "== Reporte de entregas ==";
    public final static String IN_SUFFIX = "in";
    public final static String OUT_SUFFIX = "out";
}
