package com.s4n;

import com.s4n.behavior.DoDeliveryImpl;
import com.s4n.behavior.DroneMovement;
import com.s4n.behavior.Invoker;
import com.s4n.constants.Constants;
import com.s4n.utils.ReadAndCreateFile;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Clase principal de la app.
 *
 * @author Andrés Peña Mantilla
 */
public class DroneRoad {

    private final static Logger log = Logger.getLogger(DroneRoad.class);


    public static void main(String[] args) {

        //Lista que contiene los recorridos para cada archivo, se ejecuta mediante hilos lo que garantiza la simultaneidad
        // Requiere Java 8 y la cantidad de Threads depende del numero de núcleos e hilos por núcleo del procesador (CPU)

        IntStream range2 = IntStream.rangeClosed(1, Constants.NUMBER_OF_DRONES);
            range2.parallel().forEach( i -> {

                ReadAndCreateFile readAndCreateFile = new ReadAndCreateFile();
                DroneMovement droneMovement = new DroneMovement();

                //Se asigna la lista de recorridos por cada archivo (Dron)
                List<String> droneRoad = readAndCreateFile.readTxt(i, Constants.INPUT_FOLDER);
                //Llamado del patrón command, al que se le envía el comando a ejecutar
                DoDeliveryImpl doDelivery = new DoDeliveryImpl(droneMovement, droneRoad, i);
                Invoker invoker = new Invoker();
                //Ejecuta el comando en este caso hacer la entrega de cada dron
                invoker.recieveMovement(doDelivery);
                invoker.doDroneMovements();

                long threadId = Thread.currentThread().getId();
                log.info("Thread # " + threadId + " is doing this task");
        });

        //Fix
        // droneRoad.add("AAAAIAAD");
        // droneRoad.add("DDADAI");
        // droneRoad.add("AADADAD");
    }
}
