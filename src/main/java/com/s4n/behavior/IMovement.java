package com.s4n.behavior;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

//Command Pattern
@FunctionalInterface
public interface IMovement {
    void execute() throws Exception;
}
