package com.s4n.behavior;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


/**
 * Clase invocadora
 *
 * @author Andrés Peña Mantilla
 */
public class Invoker {

    //Lista de movimientos a ejecutar
    private List<IMovement> movements = new ArrayList<>();

    public void recieveMovement(IMovement movement){
        this.movements.add(movement);
    }

    //Ejecuta la interface funcional independientemente de cual sea su implementación en este caso es execute
    public void doDroneMovements(){

        //Ejecuta el metodo execute de la lista que es de tipo Interfaz funcional
        this.movements.forEach(x -> {
            try {
                x.execute();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
