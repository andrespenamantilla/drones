package com.s4n.behavior;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Clase implementación de la interface funcional ( Patrón command)
 *
 * @author Andrés Peña Mantilla
 */
public class DoDeliveryImpl implements IMovement {
    //Objeto que hace mover al dron
    private DroneMovement droneMovement;
    //Lista de movimientos que vienen de los archivos inXX.txt
    private List<String> droneDelivery;
    //índice que indica cual es el archivo que se está leyendo
    private Integer index;

    public DoDeliveryImpl(DroneMovement droneMovement, List<String> droneDelivery, Integer index) {
        this.droneMovement = droneMovement;
        this.droneDelivery = droneDelivery;
        this.index = index;
    }

    //Aquí ejecuta los movimientos, los cuales estan representados en una lista de strings
    @Override
    public void execute() throws Exception {
        this.droneMovement.doMovement(droneDelivery, index);
    }
}
