package com.s4n.behavior;

import com.s4n.constants.Constants;
import com.s4n.constants.MovementCommand;


/**
 * Clase encargada de realizar el cálculo de la dirección del dron.
 *
 * @author Andrés Peña Mantilla
 */
public class DroneSpin {

    public String actual_position_spin;


    //Esta función calcula el griro del dron
    public String calculateSpin(String position, MovementCommand movementRightOrLeft) {

        // Calculate for left movements

        if (position.equals(Constants.UP) && movementRightOrLeft.equals(MovementCommand.I)) {
            actual_position_spin = Constants.LEFT;
            return actual_position_spin;
        }
        if (position.equals(Constants.RIGHT) && movementRightOrLeft.equals(MovementCommand.I)) {
            actual_position_spin = Constants.UP;
            return actual_position_spin;
        }

        if (position.equals(Constants.DOWN) && movementRightOrLeft.equals(MovementCommand.I)) {
            actual_position_spin = Constants.RIGHT;
            return actual_position_spin;
        }

        if (position.equals(Constants.LEFT) && movementRightOrLeft.equals(MovementCommand.I)) {
            actual_position_spin = Constants.DOWN;
            return actual_position_spin;
        }

        // Calculate for right movements

        if (position.equals(Constants.UP) && movementRightOrLeft.equals(MovementCommand.D)) {
            actual_position_spin = Constants.RIGHT;
            return actual_position_spin;
        }
        if (position.equals(Constants.RIGHT) && movementRightOrLeft.equals(MovementCommand.D)) {
            actual_position_spin = Constants.DOWN;
            return actual_position_spin;
        }
        if (position.equals(Constants.DOWN) && movementRightOrLeft.equals(MovementCommand.D)) {
            actual_position_spin = Constants.LEFT;
            return actual_position_spin;
        }
        if (position.equals(Constants.LEFT) && movementRightOrLeft.equals(MovementCommand.D)) {
            actual_position_spin = Constants.UP;
            return actual_position_spin;
        }

        return actual_position_spin;
    }

    public String getActual_position_spin() {
        return actual_position_spin;
    }

    public void setActual_position_spin(String actual_position_spin) {
        this.actual_position_spin = actual_position_spin;
    }
}