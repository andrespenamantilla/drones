package com.s4n.behavior;

import com.s4n.constants.Constants;
import com.s4n.constants.MovementCommand;
import com.s4n.exception.MovementDoesntExistException;
import com.s4n.exception.NumberOfDinnerException;
import com.s4n.exception.RangeDeliveryException;
import com.s4n.pojo.Position;
import com.s4n.utils.ReadAndCreateFile;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase principal de la app.
 *
 * @author Andrés Peña Mantilla
 */

public class DroneMovement {

    private final static Logger log = Logger.getLogger(DroneMovement.class);

    //Objeto que tiene la posición X , Y y la orientación ( Dirección del dron)
    private Position position;

    //Objeto que contiene la función para calcular la dirección del dron
    private DroneSpin droneSpin;

    public DroneMovement() {
        droneSpin = new DroneSpin();
        position = new Position();
    }

    //función que se encarga de hacer el movimiento
    public void doMovement(List<String> droneDelivery, Integer index) throws Exception {
        ReadAndCreateFile readAndCreateFile = new ReadAndCreateFile();
        log.info(droneDelivery);
        //Lista que contiene una linea de caracteres separados para ejecutarse de uno en uno
        List<List<Character>> allDeliveryLine = new ArrayList<>();

        // Transformación de una linea de Strings a un arreglo de Character's
        droneDelivery.forEach(line -> {
            allDeliveryLine.add(deliveryLinePerMovement(line));
        });

        //Aquí genera una excepción cuando el dron tiene mas de tres entregas por lo cual no genera el archivo txt
        if(allDeliveryLine.size() >3){
            try {
                throw new NumberOfDinnerException();
            }catch (NumberOfDinnerException e){
                e.printStackTrace();
            }
        }

        //Si el dron tiene tres paquetes o menos , realiza la escritura del archivo
        if (allDeliveryLine.size() <= 3) {
            List<Position> positions = new ArrayList<>();
            Position positionDelivery;
            //Mediante este ciclo asigna a una lista las posiciones de los puntos cardinales ya encontrados
            for (int i = 0; i < allDeliveryLine.size(); i++) {
                positionDelivery = obtainPositionOnCastesianPlane(allDeliveryLine.get(i), i);
                positions.add(new Position(positionDelivery.x_value, positionDelivery.y_value, positionDelivery.direction));
            }
            //Aquí crea los archivos con la lista de puntos encontrados
            //index indica el número XX con el cual se va a crear el archivo de salida
            readAndCreateFile.createTxt(index, positions, Constants.OUTPUT_FOLDER);
        }


    }

    //Este metodo vuelve una linea de entrega es decir DDAAAAAA en una lista de comandos separados para ser ejecutados es decir D,D,A,A,A,A,A
    public List<Character> deliveryLinePerMovement(String deliveryLine) {
        List<Character> deliveryLinePerMovement = new ArrayList<>();

        for (Character c : deliveryLine.toCharArray()) {
            deliveryLinePerMovement.add(c);
        }
        return deliveryLinePerMovement;
    }

    // Esta es la función es la que se encarga de determinar como se obtiene el punto cartesiano en el plano
    public Position obtainPositionOnCastesianPlane(List<Character> droneLineDeliveryPackage, Integer index) throws Exception{

       try {
           //Aquí indica que la pocisión del dron en la primera entrega es (0,0,N)
           if (index == 0) {
               position.setX_value(0);
               position.setY_value(0);
               droneSpin.setActual_position_spin(Constants.UP);
           }

           //Si el movimiento no existe entre D,A,I entonces dispara una excepción y no lo calcula dentro del flujo de entrega
           droneLineDeliveryPackage.forEach(movement -> {

               if(!MovementCommand.contains(movement.toString())){
                   try {
                       throw  new MovementDoesntExistException();
                   } catch (MovementDoesntExistException e) {
                       e.printStackTrace();
                   }
               }

               if (movement.toString().equals(MovementCommand.D.toString()) || movement.toString().equals(MovementCommand.I.toString())) {
                   droneSpin.calculateSpin(droneSpin.getActual_position_spin(), MovementCommand.valueOf(movement.toString()));
               }

               //Evalua la condición si el movimiento es avanzar y en base al avance y la dirección suma o resta la dirección en el plano X y Y
               boolean movementProgress = movement.toString().equals(MovementCommand.A.toString());
               if (droneSpin.getActual_position_spin().equals(Constants.UP) && movementProgress) {
                   position.y_value = position.y_value + 1;
               }

               if (droneSpin.getActual_position_spin().equals(Constants.RIGHT) && movementProgress) {
                   position.x_value = position.x_value + 1;
               }

               if (droneSpin.getActual_position_spin().equals(Constants.DOWN) && movementProgress) {
                   position.y_value = position.y_value - 1;
               }

               if (droneSpin.getActual_position_spin().equals(Constants.LEFT) && movementProgress) {
                   position.x_value = position.x_value - 1;
               }

               // Si supera el rango de 10 Cuadras en el plano X positivo lanza una excepción

               if (position.getX_value() > Constants.RANGE_DELIVERY) {
                   position = null;
                   try {
                       throw new RangeDeliveryException();
                   } catch (RangeDeliveryException e) {
                       e.printStackTrace();
                   }
               }

               // Si supera el rango de 10 Cuadras en el plano X negativo lanza una excepción
           if (position.getX_value() < -Constants.RANGE_DELIVERY) {
               position = null;
               try {
                   throw new RangeDeliveryException();
               } catch (RangeDeliveryException e) {
                   e.printStackTrace();
               }
           }

           // Si supera el rango de 10 Cuadras en el plano Y negativo lanza una excepción
           if (position.getY_value() < -Constants.RANGE_DELIVERY) {
               position = null;
               try {
                   throw new RangeDeliveryException();
               } catch (RangeDeliveryException e) {
                   e.printStackTrace();
               }
           }

           // Si supera el rango de 10 Cuadras en el plano Y positivo lanza una excepción
           if (position.getY_value() >Constants.RANGE_DELIVERY) {
               position = null;
               try {
                   throw new RangeDeliveryException();
               } catch (RangeDeliveryException e) {
                   e.printStackTrace();
               }
           }
       });

           log.info("Value in X " + position.getX_value());
           log.info("Value in Y " + position.getY_value());
           log.info("The Drone Direccion is " + droneSpin.getActual_position_spin());
           position.setDirection(droneSpin.getActual_position_spin());

           return position;

       }catch (Exception e){

       }
       return  null;
    }

}