package com.s4n;

import com.s4n.behavior.DroneMovement;
import com.s4n.constants.Constants;
import com.s4n.pojo.Position;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class LongDistanceRangeAndNotValidCommandTest {

    private List<Character> rangeX;
    private List<Character> rangeY;
    private List<Character> rangeMinusX;
    private List<Character> rangeMinusY;
    private List<Character> commandNotValid;


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        initialize();
    }

    @After
    public void tearDown() {
    }


    //Falla por que el rango no es permitido
    @Test
    public void rangeInXShouldFail(){
        try {
            DroneMovement droneMovement = new DroneMovement();
            droneMovement.obtainPositionOnCastesianPlane(rangeX, 0);
            fail();
        } catch (Exception e) {
        }
    }

    //Falla por que el rango no es permitido
    @Test
    public void rangeInYShouldFail() {
        try {
            DroneMovement droneMovement = new DroneMovement();
            droneMovement.obtainPositionOnCastesianPlane(rangeY, 0);
            fail();
        } catch (Exception e) {
        }

    }

    //Falla por que el rango no es permitido
    @Test
    public void rangeInMinusXShouldFail() {
        try {
            DroneMovement droneMovement = new DroneMovement();
            droneMovement.obtainPositionOnCastesianPlane(rangeMinusX, 0);
            fail();
        } catch (Exception e) {
        }
    }
    //Falla por que el rango no es permitido
    @Test
    public void rangeInMinusYShouldFail() {
        try {
            DroneMovement droneMovement = new DroneMovement();
            droneMovement.obtainPositionOnCastesianPlane(rangeMinusY, 0);
            fail();
        } catch (Exception e) {
        }
    }
    //No falla, pero omite la X porque no es un comando y encuentra la posición esperada
    @Test
    public void commandNotValidShouldContinue() throws Exception {
           try {
               DroneMovement droneMovement = new DroneMovement();
               Position position = droneMovement.obtainPositionOnCastesianPlane(commandNotValid, 0);
               assertTrue(position.x_value == 0 );
               assertTrue(position.y_value == -4 );
               assertTrue(position.getDirection().equals(Constants.DOWN) );
           }catch (Exception e){

           }
    }

    public void initialize() {
        rangeX = new ArrayList<>();
        rangeY = new ArrayList<>();
        rangeMinusX = new ArrayList<>();
        rangeMinusY = new ArrayList<>();
        commandNotValid = new ArrayList<>();

        rangeY.add('D');
        rangeY.add('D');
        rangeY.add('D');

        rangeMinusX.add('D');
        rangeMinusX.add('D');

        rangeMinusY.add('I');
        commandNotValid.add('D');
        commandNotValid.add('D');
        commandNotValid.add('A');
        commandNotValid.add('A');
        commandNotValid.add('X');
        commandNotValid.add('A');
        commandNotValid.add('A');


        Character a = 'A';
        for (int i = 0; i < 11; i++) {
            rangeY.add(a);
            rangeX.add(a);
            rangeMinusY.add(a);
            rangeMinusX.add(a);
        }
    }
}
