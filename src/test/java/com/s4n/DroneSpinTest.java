package com.s4n;


import com.s4n.behavior.DroneSpin;
import com.s4n.constants.Constants;
import com.s4n.constants.MovementCommand;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DroneSpinTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void moveWithLeftMovementFrom0Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.UP,MovementCommand.I);
        assertEquals(actualPositionSpin,Constants.LEFT);
    }


    @Test
    public void moveWithLeftMovementFrom90Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.RIGHT,MovementCommand.I);
        assertEquals(actualPositionSpin,Constants.UP);
    }


    @Test
    public void moveWithLeftMovementFrom180Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.DOWN,MovementCommand.I);
        assertEquals(actualPositionSpin,Constants.RIGHT);
    }

    @Test
    public void moveWithLeftMovementFrom270Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.LEFT,MovementCommand.I);
        assertEquals(actualPositionSpin,Constants.DOWN);
    }


    @Test
    public void moveWithRightMovementFrom0Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.UP,MovementCommand.D);
        assertEquals(actualPositionSpin,Constants.RIGHT);
    }


    @Test
    public void moveWithRightMovementFrom90Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.RIGHT,MovementCommand.D);
        assertEquals(actualPositionSpin,Constants.DOWN);
    }


    @Test
    public void moveWithRightMovementFrom180Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.DOWN, MovementCommand.D);
        assertEquals(actualPositionSpin,Constants.LEFT);
    }

    @Test
    public void moveWithRightMovementFrom270Degrees(){
        DroneSpin droneSpin = new DroneSpin();
        String actualPositionSpin = droneSpin.calculateSpin(Constants.LEFT,MovementCommand.D);
        assertEquals(actualPositionSpin,Constants.UP);
    }
}
