# Prueba Técnica S4N

Para este caso la prueba consiste en realizar el la implementación de la entrega de paquetes/almuerzos por 20 drones
con un rango de 10 cuadras a 3 paquetes por dron.

Basándome en mi razonamiento del enunciado puuedo indentificar que las coordenadas 

AAAAIAA
DDDAIAD
AAIADAD

con reporte de entregas:

== Reporte de entregas ==
(-2, 4) dirección Norte
(-3, 3) dirección Sur
(-4, 2) dirección Oriente

Está erroneo ya que para que el reporte indique esas coordenadas el fix quedaría de esta manera:

AAAAIAAD
DDADAI
AADADAD

Según esto se dan soluciones a 5 problematicas:

- Giro del dron
- Límite de cuadras a la redonda
- Entrega de máximo 3 almuerzos
- Movimiento de 20 drones en simultaneo
- Lectura y escritura de archivos de entrada y salida


IMPORT Y BUILD

Debido a que es un proyecto maven en cualquier IDE solo hay que seleccionar el import como maven project y
esperar que descargue las dependencias y esta listo. (hacer mvn clean, mvn build y mvn install si se hace manualmente)


EJECUCIÓN

Es sencillo despues de importar el proyecto ir a DroneRoad.java y Correr la App.

CORRER LOS TEST

Ir a la clase DroneSpinTest.java  y LongDistanceRangeAndNotValidCommandTest.java y correr todas las pruebas unitarias para los
escenarios planteados.

VIDEO
El video esta en este repositorio, como me fue indicado tenía que durar máximo 3 minutos, despues de eso no serían calificados
las justificaciones despues de ese tiempo, dado el caso cada función tiene su comentario respecto a la función que cumple dentro del programa

IMPORTANTE JAVA8 ES NECESARIO , YA QUE USA LOOPS Y THREADS DE ESA JDK
 